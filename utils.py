import sys
import os

class FileScanner:
    exts_video = ('.mp4', '.mov', '.avi', '.mpg', '.mpeg', '.rm', '.rmvb', '.wmv')
    exts_image = ('.jpg', '.png')

    def __init__(self, exts):
        self.exts = exts
    
    def scan(self, target_dir):
        result = []
        for root, dirs, files in os.walk(target_dir):
            for name in files:
                for ext in self.exts:
                    if name.lower().endswith(ext.lower()):
                        result.append(os.path.join(root, name))
        return tuple(result)