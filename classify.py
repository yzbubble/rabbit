
import sys
import os
import shutil
import subprocess
import json
import argparse
import uuid
import utils

def parse_argv():
    parser = argparse.ArgumentParser(description="Classify files by resolution.")
    parser.add_argument("-i", "--input", nargs=1, help="input root directory.", required=True)
    parser.add_argument("-o", "--output", nargs=1, help="output root directory.")
    parser.add_argument("-m", "--mode", nargs=1, help="operate file mode.", choices=["copy", "move"], default=["copy"])
    parser.add_argument("-t", "--target", nargs=1, help="operation target file type.", choices=["image", "video"], default=["video"])
    parser.add_argument("-e", "--exts", nargs="+", help="target file extention list.")
    parser.add_argument("-P", "--disable_protect", action="store_true", help="whether to disable the protection directory.")
    args = parser.parse_args()
    print(args)
    return args

def classify(input, output, mode="copy", target="video", exts=None, disable_protect=False):
    if not exts:
        if target == "video":
            exts = utils.FileScanner.exts_video
        elif target == "image":
            exts = utils.FileScanner.exts_image
        else:
            raise RuntimeError('parameter "target" value incorrect.')
    
    if not disable_protect:
        output = os.path.join(output, str(uuid.uuid1()))

    paths = utils.FileScanner(exts).scan(input)

    for path in paths:
        try:
            resolution = FFProbe(path).get_resolution()
            level = ResolutionClassifier.test(resolution)

            fdir = os.path.join(output, level)
            if not os.path.exists(fdir):
                os.makedirs(fdir)
            fname = os.path.split(path)[1]
            outpath = os.path.join(fdir, fname)
        except Exception as e:
            print("classify %s failed: %s" % (path, e))
            continue
        
        if mode == "copy":
            try:
                print("copy %s -> %s ..." % (path, outpath))
                shutil.copyfile(path, outpath)
                print("copy %s -> %s done." % (path, outpath))
            except Exception as e:
                print("copy %s failed: %s" % (path, e))
        elif mode == "move":
            try:
                print("move %s -> %s ..." % (path, outpath))
                shutil.move(path, outpath)
                print("move %s -> %s done." % (path, outpath))
            except Exception as e:
                print("move %s failed: %s" % (path, e))


class ResolutionClassifier:
    @staticmethod
    def test(resolution):
        width = int(resolution[0])
        height = int(resolution[1])
        if width >= 1920 and height >= 1080:
            return "fhd"
        elif width >= 1080 and height >= 720:
            return "hd"
        else:
            return "sd"

class FFProbe:
    def __init__(self, fileName):
        self.fileName = fileName

    def get_ffprobe_json(self):
        cmd = ["ffprobe.exe", "-loglevel", "quiet", "-print_format", "json", "-show_format", "-show_streams", "-i", self.fileName]
        result = subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        out = result.stdout.read()
        temp = str(out.decode("utf-8"))
        return json.loads(temp)

    def get_resolution(self):
        vjson = self.get_ffprobe_json()
        viter = filter(lambda x: x["codec_type"] == "video", vjson["streams"])
        vinfo = next(viter)
        return (vinfo["width"], vinfo["height"])
    
    def get_duration(self):
        vjson = self.get_ffprobe_json()
        viter = filter(lambda x: x["codec_type"] == "video", vjson["streams"])
        vinfo = next(viter)
        return vinfo["duration"]


if __name__ == '__main__':
    args = parse_argv()
    classify(
        input=args.input[0],
        output=args.output[0] if args.output else args.input[0],
        mode=args.mode[0],
        target=args.target[0],
        exts=args.exts,
        disable_protect=args.disable_protect
    )