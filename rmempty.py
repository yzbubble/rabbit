import sys
import os
import shutil
import subprocess
import json
import argparse

def parse_argv():
    parser = argparse.ArgumentParser(description="remove empty folder or empty file.")
    parser.add_argument("-i", "--input", nargs=1, help="input root directory.", required=True)
    args = parser.parse_args()
    print(args)
    return args

def rmempty(input):
    for root, dirs, filenames in os.walk(input, topdown=False):
        for filename in filenames:
            filepath = os.path.join(root, filename)
            if not os.path.getsize(filepath):
                os.remove(filepath)
        for dir in dirs:
            dirpath = os.path.join(root, dir)
            if not os.listdir(dirpath):
                os.rmdir(dirpath)
    
if __name__ == '__main__':
    args = parse_argv()
    rmempty(args.input[0])