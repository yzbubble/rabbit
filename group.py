import sys
import os
import shutil
import subprocess
import json
import argparse
import uuid
import utils

def parse_argv():
    parser = argparse.ArgumentParser(description="group files.")
    parser.add_argument("-i", "--input", nargs=1, help="input root directory.", required=True)
    parser.add_argument("-o", "--output", nargs=1, help="output root directory.")
    parser.add_argument("-m", "--mode", nargs=1, help="operate file mode.", choices=["copy", "move"], default=["copy"])
    parser.add_argument("-t", "--target", nargs=1, help="operation target file type.", choices=["image", "video"], default=["image"])
    parser.add_argument("-e", "--exts", nargs="+", help="target file extention collection.")
    parser.add_argument("-s", "--size", nargs=1, help="group size(default 100).", default=[100], type=int)
    parser.add_argument("-r", "--rename", action="store_true", help="whether to rename.")
    parser.add_argument("-P", "--disable_protect", action="store_true", help="whether to disable the protection directory.")
    args = parser.parse_args()
    print(args)
    return args

def group(input, output, mode="copy", target="image", exts=None, group_size=100, rename=False, disable_protect=False):
    if not exts:
        if target == "video":
            exts = utils.FileScanner.exts_video
        elif target == "image":
            exts = utils.FileScanner.exts_image
        else:
            raise RuntimeError('parameter "target" value incorrect.')

    if not disable_protect:
        output = os.path.join(output, str(uuid.uuid1()))

    paths = utils.FileScanner(exts).scan(input)
    group_count, file_count = 0, 0

    for path in paths:
        file_count += 1
        if file_count % group_size == 1:
            group_count += 1
        parent_dir = "G%s" % (str(group_count).zfill(4),)
        target_dir = os.path.join(output, parent_dir)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        target_name = os.path.split(path)[1]
        if rename:
            target_name = "%sS%s%s" % (parent_dir, str((file_count % group_size) or group_size).zfill(4),os.path.splitext(path)[1])
        target_full_path = os.path.join(target_dir, target_name)

        if mode == "copy":
            shutil.copyfile(path, target_full_path)
        elif mode == "move":
            shutil.move(path, target_full_path)

if __name__ == '__main__':
    args = parse_argv()
    group(
        input=args.input[0],
        output=args.output[0] if args.output else args.input[0],
        mode=args.mode[0],
        target=args.target[0],
        exts=args.exts,
        group_size=args.size[0],
        rename=args.rename,
        disable_protect=args.disable_protect
    )